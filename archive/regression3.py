from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression 
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns

data=pd.read_csv(r'Salary_Data.csv',sep=",")
plt.scatter('YearsExperience','Salary',data=data)
plt