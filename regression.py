import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor
##Chargement des données

donnees_boston_df=pd.read_csv(r'boston.csv',sep=",")


#On regarde si il n'y a pas de valeur nulle
print(donnees_boston_df.isnull().sum())

A=(donnees_boston_df.isnull().sum())

#MATRICE DE CONFUSION

matrice_corr = donnees_boston_df.corr().round(1)
sns.heatmap(data=matrice_corr, annot=True), 
X=pd.DataFrame(np.c_[donnees_boston_df['lstat'],donnees_boston_df['rm'],donnees_boston_df['tax'],donnees_boston_df['ptratio']], columns = ['LSTAT','RM','TAX','PTRATIO'])
Y = donnees_boston_df['medv']


#base d'apprentissage et base de test
from sklearn.model_selection import train_test_split
 
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2, random_state=5)
print(f'X train shape: {X_train.shape}')
print(f'X test shape: {X_test.shape}')
print(f'Y train shape: {Y_train.shape}')
print(f'Y test shape: {Y_test.shape}\n')



from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
 
lmodellineaire = LinearRegression()
lmodellineaire.fit(X_train, Y_train)

print(f'Params: {lmodellineaire.get_params()}')
print(f'Score: {lmodellineaire.score(X_train, Y_train)}')


from sklearn.metrics import r2_score
y_train_predict = lmodellineaire.predict(X_train)
rmse = (np.sqrt(mean_squared_error(Y_train, y_train_predict)))
r2 = r2_score(Y_train, y_train_predict)
 
print('La performance du modèle sur la base dapprentissage')
print('--------------------------------------')
print('Lerreur quadratique moyenne est {}'.format(rmse))
print('le score R2 est {}'.format(r2))
print('\n')
 
# model evaluation for testing set
sklearn_y_test_predict = lmodellineaire.predict(X_test)
# print(f'Sklearn predict: {sklearn_y_test_predict}')
rmse = (np.sqrt(mean_squared_error(Y_test, sklearn_y_test_predict)))
r2 = r2_score(Y_test, sklearn_y_test_predict)

# generate x and y
""" x = np.linspace(0, 1, 101)
y = 1 + x + x * np.random.random(len(x))
# turn y into a column vector
y = y[:, np.newaxis]
print(f'Y: {y}') """


print(X_train)
A = np.vstack([X_train.LSTAT.values, np.ones(len(X_train.LSTAT.values))]).T
# turn y into a column vector
Y_train = Y_train[:, np.newaxis]
# print(f'Y_train: {Y_train}')
# Direct least square regression
alpha = np.dot((np.dot(np.linalg.inv(np.dot(A.T,A)),A.T)),Y_train)
# print(f'Alpha: {alpha}')
# print(A)
""" x = np.linspace(0, 1, 101)
A_2 = np.vstack([x, np.ones(len(x))]).T
print(A_2)
print(f'x: {x}')
print(f'X_train: {X_train.LSTAT.values}') """

 
print('La performance du modèle sur la base de test')
print('--------------------------------------')
print('Lerreur quadratique moyenne est {}'.format(rmse))
print('le score R2 est {}'.format(r2))



#VIF facte


vif = pd.DataFrame()
vif["VIF Factor"] = [variance_inflation_factor(X.values, i) for i in range(X.shape[1])]
vif["features"] = X.columns
vif.round(1)