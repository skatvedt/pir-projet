from math import sqrt
import numpy as np
from numpy.linalg import eig
from sklearn.preprocessing import normalize
from normal_reader import read_matrix

# Input is Numpy array
N = read_matrix()

# Normalize matrix
N_norm = normalize(N,norm="l1",axis=1)

# Eigenvalues and Eigenvector
N_norm_eigenvalues, N_norm_eigenvectors = eig(N_norm)

# Transposed Eigenvector
N_norm_eigenvectors_transpose = np.transpose(N_norm_eigenvectors)

# Diagonal eigenvalue matrix
diagonal=np.diag(N_norm_eigenvalues)

# Calculate diag_max/diag_i matrix
max_eigen = N_norm_eigenvalues.max()
mu_vector = np.array([])
value = N_norm_eigenvalues[406]
for value in N_norm_eigenvalues:
    mu_vector = np.append(mu_vector, sqrt(max_eigen / value))
print(mu_vector)