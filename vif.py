import pandas as pd
from statsmodels.stats.outliers_influence import variance_inflation_factor
from normal_reader import read_matrix




# VIF dataframe
vif_data = pd.DataFrame()

# calculating VIF for each feature
vif_data["VIF"] = [variance_inflation_factor(read_matrix(), i)
						for i in range(77775)]